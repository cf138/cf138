
#include <stdio.h>
int main()
{
    int a[10][10], transpose[10][10], r, c, i, j;
    printf("Enter rows and columns: ");
    scanf("%d %d", &r, &c);
    printf("\nEnter matrix elements:\n");
    for (j = 0; j < c;j++)
        for (i = 0; i< r; i++)
	{
            printf("Enter element a%d%d: ", j + 1, i + 1);
            scanf("%d", &a[j][i]);
    }
        printf("\nEntered matrix: \n");
       for (j = 0; j < c; j++)
           for (i = 0; i < r; i++)
		   {
               printf("%d  ", a[j][i]);
               if (i == r - 1)
                   printf("\n");
           }
           for (j = 0; j < r;j++)
       for (i = 0; i< c; i++)
	   {
           transpose[j][i] = a[i][j];
       }
       printf("\nTranspose of the matrix:\n");
    for (j = 0; j < r; j++)
        for (i = 0; i < c; i++)
		{
            printf("%d  ", transpose[i][j]);
            if (i == c - 1)
                printf("\n");
        }
    return 0;
}
