#include<stdio.h>
int main()
{
    int i,pos,flag=0,key;
    int a[6]={2,4,6,8,10,12};
    printf(" The array elements in the ascending order\n");
    for(i=0;i<6;i++)
    {
        printf("%d\n",a[i]);
    }
    printf("Enter the element to be searched in the above array\n");
    scanf("%d",&key);
    int beg=0,end=(5),mid;
    while(beg<=end)
    {
        mid=(beg+end)/2;
        if(a[mid]==key)
        {
            flag=1;
            pos=mid+1;
        }
        if(a[mid]<key)
        {
            beg=mid+1;
        }
        else
        {
            end=mid-1;
        }
    }
        if(flag==1)
        {
            printf("Search Successful and the position of the searched element %d is given by %d\n",key,pos);
        }
        else
        {
            printf("Search Unsuccessful\n");
        }
        return 0;
    }
