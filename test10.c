#include<stdio.h>
int main()
{
  int x,y,*px,*py,sum,sub,rem,mul,div;
  px=&x;
  py=&y;
  printf("enter the values of x and y\n");
  scanf("%d%d",px,py);
  sum=*px+*py;
  sub=*px-*py;
  mul=*px**py;
  div=(*px)/(*py);
  rem=(*px)%(*py);
  printf("%d+%d=%d\n",*px,*py,sum);
  printf("%d-%d=%d\n",*px,*py,sub);
  printf("%d*%d=%d\n",*px,*py,mul);
  printf("%d/%d=%d\n",*px,*py,div);
  printf("%d%%d=%d\n",*px,*py,rem);
  return 0;
}